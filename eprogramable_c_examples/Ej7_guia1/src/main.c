/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Sobre una variable de 32 bits sin signo previamente declarada
 * y de valor desconocido, asegúrese de colocar el bit 3 a 1 y
 * los bits 13 y 14 a 0 mediante máscaras y el operador <<.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Ej7_guia1/inc/main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

#define BIT_3 3
#define BIT_13 13
#define BIT_14 14
uint32_t variable = 0xFFFF;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint32_t mascara_ceros = (1 <<BIT_14) | (1<<BIT_13); //declaro una máscara de 32 bits con ceros, y un 1 en el bit 13 y en el bit 14
	mascara_ceros = ~mascara_ceros; //se invierten los bits quedando todos 1 y un 0 en el bit 13 y en el bit 14.

	printf("Mascara ceros: %u \n", mascara_ceros); //se muestra la primer mascara

	uint32_t mascara_uno = (1 <<BIT_3); //declaro una máscara de 32 bits con todos ceros y un 1 en el bit 3
	printf("Mascara uno: %u \n", mascara_uno); //se muestra la mascara


	variable = variable & mascara_ceros; //realizo un operacion AND con la variable previamente declarada y la mascara de ceros, para colocar los 1 en bit 13 y 14
	variable = variable | mascara_uno; //realizo una operacion OR con la variable y la mascara de unos, para colocar el 1 en el bit 3
	printf("Valor: %u", variable);//muestro la variable final con todas las modificaciones
	return 0;
}

/*==================[end of file]============================================*/

