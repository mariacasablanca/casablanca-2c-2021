/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*Declare un puntero a un entero con signo de 16 bits y
 * cargue inicialmente el valor -1. Luego, mediante máscaras, coloque un 0 en el bit 4.*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

#define BIT_4 4
int16_t valor = -1;
int16_t *puntero_valor;


/*==================[internal functions declaration]=========================*/

int main(void)
{
	puntero_valor = &valor; //le asigno a un puntero de 16 bits la direccion de la variable valor

	printf("Valor: %d \n", valor);

	printf("Puntero: %d \n", *puntero_valor);

	int16_t mascara = 1 << BIT_4; //declaro una variable máscara de 16 bits con ceros y un 1 en el bit 4

	mascara = ~mascara; //niego la variable máscara, para así obtener una máscara completa con 1 y un 0 en el bit 4

	*puntero_valor = *puntero_valor & mascara; //se modifica la variable a la cual apunta este puntero, colocando un 0 en el bit 4 mediante
	//la mascara y una operación AND.

	printf("Puntero: %d \n", *puntero_valor); //se muestra la variable modificada.




	return 0;
}

/*==================[end of file]============================================*/


