/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo  gpioConf_t.

typedef struct
{
	uint8_t port;
	uint8_t pin;
	uint8_t dir;
} gpioConf_t;

Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14


La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector anterior operar sobre el puerto y pin que corresponda.
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/


#define FACTOR_10 10
#define CANT_BITS 4


void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t i = 0, unidad_aux;
	uint32_t num_aux = data;

	for(i=0; i<digits; i++){

		unidad_aux = (num_aux%FACTOR_10);
		num_aux = (num_aux/FACTOR_10);
		bcd_number[i] = unidad_aux;

	}

}


typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

void Display_7seg (uint8_t digito_bcd, gpioConf_t vector[4]){

	uint8_t pos;
	uint8_t mascara = 0;
	for(pos = 0; pos < CANT_BITS; pos++){
		mascara = 1 << pos;

		if(mascara & digito_bcd){
			printf("SI prende led: \n");
			printf("Puerto: %u \n", vector[pos].port);
			printf("Pin: %u \n", vector[pos].pin);
			printf("Direccion: %u \n\n", vector[pos].dir);
		}

		else{
			printf("NO prende led:  \n");
			printf("Puerto: %u \n", vector[pos].port);
			printf("Pin: %u \n", vector[pos].pin);
			printf("Direccion: %u \n\n", vector[pos].dir);
		}

		mascara = 0;
	}

}

/*==================[internal functions declaration]=========================*/

int main(void)
{

	uint32_t dato = 25893;
	uint8_t digitos = 5;
	uint8_t puntero[digitos];
	BinaryToBcd(dato, digitos, puntero);
	uint8_t a;
	gpioConf_t vec[4] = {{1,4,1},{1,2,3},{1,6,4},{2,4,5}};

	for(a = 0; a<digitos; a++){

		printf("Digito: %u \n",puntero[a]);
		Display_7seg (puntero[a], vec);
		printf("\n");

	}






	return 0;
}

/*==================[end of file]============================================*/
