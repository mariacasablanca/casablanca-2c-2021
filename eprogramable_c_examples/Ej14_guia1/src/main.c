/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
 * caracteres y edad. Defina una variable con esa estructura y cargue los campos con sus propios datos.
 * Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso
 * por punteros).
 *
 */
/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/

typedef struct{
	 uint8_t nombre[12];
	 uint8_t apellido[20];
	 uint8_t edad;

} Alumno; //Se define una estructura de tipo alumno, que contiene un nombre, un apellido y una edad.


/*==================[internal functions declaration]=========================*/

int main(void)
{
	Alumno alumno_1, *alumno_2; //se declara una variable de tipo alumno y un puntero que apunta a una variable de tipo alumno.


	strcpy(alumno_1.nombre, "Maria"); //Se carga el nombre del alumno_1 haciendo uso de una funcion de la libreria string.h
	strcpy(alumno_1.apellido, "Casablanca"); //Se carga el apellido del alumno_1
	alumno_1.edad = 21; //Se carga la edad del alumno_1

	printf("Nombre 1: %s \n", alumno_1.nombre); //Se muestran los datos del alumno_1
	printf("Apellido 1: %s \n", alumno_1.apellido);
	printf("Edad 1: %u \n", alumno_1.edad);

	alumno_2 = &alumno_1; //se le asigna la direccion de alumno_1 al puntero alumno_2



	strcpy(alumno_2->nombre, "Juan"); //Se cargan los datos del alumno 2. Esto modifica tambien los datos del alumno_1, ya que el puntero alumno_2 apunta a la direccion de memoria de alumno_1.
	strcpy(alumno_2->apellido, "Grigolatto");
	alumno_2->edad = 21;

	printf("Nombre 2: %s \n", alumno_2->nombre); //Se muestran los datos del alumno 2
	printf("Apellido 2: %s \n", alumno_2->apellido);
	printf("Edad 2: %u \n", alumno_2->edad);



	/* Otro método para cargar palabras:

	  char nombre_aux1[12] = {'M', 'a', 'r', 'i', 'a','\0'};
		char apellido_aux1[20] = {'C', 'a', 's', 'a', 'b', 'l', 'a', 'n', 'c', 'a','\0'};
		uint8_t i;
		for(i =0; i<12;i++){
			alumno_1.nombre[i] = nombre_aux1[i];
		}
		for(i =0; i<20;i++){
				alumno_1.apellido[i] = apellido_aux1[i];
			}

		alumno_1.edad = 21;*/

	/*char nombre_aux2[12] = {'J', 'u', 'a', 'n','\0'};
	char apellido_aux2[20] = {'G', 'r', 'i', 'g', 'o', 'l', 'a', 't', 't', 'o','\0'};

	for(i =0; i<12;i++){
		alumno_2->nombre[i] = nombre_aux2[i];
	}
	for(i =0; i<20;i++){
			alumno_2->apellido[i] = apellido_aux2[i];
		}

	alumno_2->edad = 21;*/



	return 0;
}

/*==================[end of file]============================================*/

