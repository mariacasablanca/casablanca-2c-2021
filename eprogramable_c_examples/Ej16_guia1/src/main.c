/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 * Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
 * cargue cada uno de los bytes de la variable de 32 bits.
 * Realice el mismo ejercicio, utilizando la definición de una “union”.
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/

uint32_t variable_1 = 0X01020304;
uint8_t variable_2;
uint8_t variable_3;
uint8_t variable_4;
uint8_t variable_5;
uint8_t auxiliar;

#define BIT_8 8;
#define BIT_16 16;
#define BIT_24 24;

union test{
	struct{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}cada_byte;
	uint32_t  todos_los_bytes;
};


/*==================[internal functions declaration]=========================*/

int main(void)
{

	variable_2 = (uint8_t) variable_1; //en variable_1 se guardan los 8 bits menos significativos de variable_1 utilizando truncamiento.

	auxiliar = variable_1>>BIT_8; //utilizando corrimiento se le asignan a la variable auxiliar los bits del 8 al 15 de la variable_1.
	variable_3 = auxiliar;
	auxiliar = variable_1>>BIT_16;//utilizando corrimiento se le asignan a la variable auxiliar los bits del 16 al 23 de la variable_1.
	variable_4 =  auxiliar;
	auxiliar = variable_1>>BIT_24;//utilizando corrimiento se le asignan a la variable auxiliar los bits del 24 al 31 de la variable_1.
	variable_5 =  auxiliar;

	//Se muestran las cuatro variables:

	printf("Prueba 1: \n");
	printf("Variable 1: %u \n",variable_1);
	printf("Variable 2 (Byte 0): %u \n",variable_2);
	printf("Variable 3 (Byte 1): %u \n", variable_3);
	printf("Variable 4 (Byte 2): %u \n",variable_4 );
	printf("Variable 5 (Byte 3): %u \n\n", variable_5);


	//Se hace el ejercicio utilizando union:

	 union test variable;

	 variable.todos_los_bytes = 0x01020304;

	 //Se muestran las cuatro variables:

	 printf("Prueba 2: \n");
	 printf("Byte 0: %u \n", variable.cada_byte.byte1);
	 printf("Byte 1: %u \n", variable.cada_byte.byte2);
	 printf("Byte 2: %u \n", variable.cada_byte.byte3);
	 printf("Byte 3: %u \n", variable.cada_byte.byte4);



	return 0;
}

/*==================[end of file]============================================*/

