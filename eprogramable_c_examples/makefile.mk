########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

####Ejercicio 1: guia1  ej1
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej1_guia1
#NOMBRE_EJECUTABLE = Ej1_guia1.exe

####Ejercicio 2: guia1  ej2
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej2_guia1
#NOMBRE_EJECUTABLE = Ej2_guia1.exe

####Ejercicio 3: guia1  ej3
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej3_guia1
#NOMBRE_EJECUTABLE = Ej3_guia1.exe

####Ejercicio 7: guia1  ej7
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej7_guia1
#NOMBRE_EJECUTABLE = Ej7_guia1.exe

####Ejercicio 9: guia1  ej9
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej9_guia1
#NOMBRE_EJECUTABLE = Ej9_guia1.exe

####Ejercicio 12: guia1  ej12
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej12_guia1
#NOMBRE_EJECUTABLE = Ej12_guia1.exe

####Ejercicio 14: guia1  ej14
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej14_guia1
#NOMBRE_EJECUTABLE = Ej14_guia1.exe

####Ejercicio 16: guia1  ej16
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ej16_guia1
#NOMBRE_EJECUTABLE = Ej16_guia1.exe

####Ejercicio 17: guia1  ej17
##Por el primero, se debe usar Makefile1
PROYECTO_ACTIVO = Ej17_guia1
NOMBRE_EJECUTABLE = Ej17_guia1.exe


####Ejercicio A: guia1  ej A
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = EjA_guia1
#NOMBRE_EJECUTABLE = EjA_guia1.exe


####Ejercicio C: guia1  ej C
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = EjC_guia1
#NOMBRE_EJECUTABLE = EjC_guia1.exe

####Ejercicio D: guia1  ej D
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = EjD_guia1
#NOMBRE_EJECUTABLE = EjD_guia1.exe

