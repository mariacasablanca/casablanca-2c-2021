/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0. Si es 0,
 * cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa. Para la
 * resolución de la lógica, siga el diagrama de flujo siguiente:
 *
 */


/*==================[inclusions]=============================================*/
#include "../../Ej9_guia1/inc/main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/

#define BIT_4 4
const int32_t constante = 0xFFFF;
uint32_t variable_auxiliar = 0;
uint8_t A;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint32_t mascara = 1 <<BIT_4; //declaro un máscara de ceros con un 1 en el bit 4

	printf("Mascara: %u \n", mascara);

	variable_auxiliar = constante & mascara; //guardo en una variable auxiliar el resultado de 	una operación AND entre la constante dada y la máscara anterior. Así, en caso de que el bit 4 sea 0, toda la variable será 0, pero en caso de que el bit 4 sea 1, la variable será 10000.



	printf("Variable: %u \n", variable_auxiliar);

	if(variable_auxiliar) //si la variable auxiliar es distina de cero entra al if, lo cual significa que el bit 4 es un 1, por lo tanto le asigno a A = 0xAA
	{
		A = 0xAA;
		printf("Valor de A1: %u \n", A);
	}
	else //si la variable auxiliar es cero pasa al else, lo cual significa que el bit 4 es un 0,  por lo tanto le asigno a A = 0
	{

		A = 0;
		printf("Valor de A2: %u \n", A);
	}

	return 0;
}

/*==================[end of file]============================================*/


