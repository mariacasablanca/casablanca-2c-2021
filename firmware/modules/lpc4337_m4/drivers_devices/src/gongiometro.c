/* Copyright 2021,
 * Maria Casablanca
 * mariacasabla@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*==================[inclusions]=============================================*/
#include "led.h"
#include "gpio.h"
#include "gongiometro.h"
#include "chip.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
analog_input_config analog_config;
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

/** @fn bool GongioInit(uint8_t channel);
 * @brief Inicialización del gongiómetro
 * @param[in] channel Canal analógico al cual se conecta el punto medio del potenciómetro (CH0, CH1, CH2, CH3)
 * @return TRUE si no hay error
 */
bool GongioInit(uint8_t channel){

	analog_config.input = channel;
	analog_config.mode = AINPUTS_SINGLE_READ;
	analog_config.pAnalogInput = NULL;

}

/** @fn uint8_t GongioMedirAngulo(void)
 * @brief Mide el ángulo en el cual se encuentra el medidor de distancia.
 * @param[in] No paramtero.
 * @return retorna el valor del ángulo en grados sexagecimales.
 */
uint8_t GongioMedirAngulo(void){

	uint16_t value;
	float angle = 0; //se define como flotante para que no haya errores en las divisiones.
	float voltage_input = 0;
	AnalogInputReadPolling(analog_config.input, &value);
	voltage_input = value*POWER_SUPPLY/TOTAL_BITS;
	angle = (voltage_input - VOLTAGE_0_DEGREE)/DELTA_ANGLE;
	return angle;
	
}
/*==================[end of file]============================================*/
