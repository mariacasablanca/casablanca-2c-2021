	/* Copyright 2021,
 * Maria Casablanca
 * mariacasabla@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef SI7007_H
#define SI7007_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Sensor_TyH Sensor Temperatura y Humedad
 ** @{ */

/** @brief Bare Metal header for Si7007
 **
 ** This is a driver for Si7007
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MC         Maria Casablanca
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211006 v0.1 initials initial version Maria Casablanca
 */

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"
#include "analog_io.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef struct {				/*!< Si7007 Inputs config struct*/
	    gpio_t select;			/*!< Determines the output on each pin*/
	    uint8_t PWM_1;		    /*!< Channel connected to the PWM1 output pin on the device*/
	    uint8_t PWM_2;	        /*!< Channel connected to the PWM2 output pin on the device*/
} Si7007_config;


/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/

/** @fn bool Si7007Init(Si7007_config *pins);
 * @brief Initialization function of Si7007.
 * @param[in] *pins
 * @return TRUE if no error.
 */
bool Si7007Init(Si7007_config *pins);

/** @fn uint16_t Si7007MeasureTemperature(void)
 * @brief Measures the current temperature
 * @param[in] No Parameter
 * @return value of temperature in °C
 */
float Si7007MeasureTemperature(void);

/** @fn uint16_t Si7007MeasureHumidity(void)
 * @brief Measures the current relative humidity
 * @param[in] No Parameter
 * @return value of relative humidity in %
 */
float Si7007MeasureHumidity(void);


/** @fn bool Si7007dEInit(Si7007_config *pins);
 * @brief deinitialization function of Si7007.
 * @param[in] *pins
 * @return TRUE if no error.
 */
bool Si7007Deinit(Si7007_config *pins);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef SI7007_H */

