/* Copyright 2021,
 * Martin Suarez
 * l.martin9988@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef DisplayITS_E0803_H
#define DisplayITS_E0803_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup DisplayITS_E0803
 ** @{ */

/** @brief Bare Metal header for DisplayITS_E0803 on EDU-CIAA NXP
 **
 ** This is a driver for DisplayITS_E0803 mounted on the board
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  MS         	Martin Suarez
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210908 v0.1 initials initial version Martin Suarez
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"
/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
/** @brief Definition of constants to reference the EDU-CIAA DisplayITS_E0803.
 */



/** \brief Definition of constants to control the EDU-CIAA DisplayITS_E0803.
 **
 **/


/*==================[external functions declaration]=========================*/

/** @fn bool DisplayITS_E0803Init(gpio_t * pins)
 * @brief Inicializacion de la funcion de la EDU-CIAA DisplayITS_E0803
 * @param[in] pins vector de gpio con la configuracion de los pins
 * @return TRUE si no hay error
 */
bool ITSE0803Init(gpio_t * pins);

/** @fn bool ITSE0803DisplayValue(uint16_t valor)
 * @brief Esta funcion muestra en el display un numero decimal de 3 digitos y lo guarda en una variable global
 * @param[in] valor indica el numero a mostrar
 * @return TRUE si no hay error
 */
bool ITSE0803DisplayValue(uint16_t valor);

/** @fn uint8_t ITSE0803ReadValue(void)
 * @brief Esta funcion devuelve el ultimo numero mostrado en el display
 * @param[in] sin parametro
 * @return TRUE si no hay error
 */
uint8_t ITSE0803ReadValue(void);

/** @fn bool ITSE0803Deinit(gpio_t * pins)
 * @brief Deinicializacion de DisplayITS_E0803
 * @param[in] pins vector de gpio con la configuracion de los pins
 * @return TRUE si no hay error
 */
bool ITSE0803Deinit(gpio_t * pins);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef DisplayITS_E0803_H */

