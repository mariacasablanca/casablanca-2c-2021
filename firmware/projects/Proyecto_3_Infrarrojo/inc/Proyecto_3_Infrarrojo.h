/*! @mainpage Proyecto 3 Infrarrojo
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de contar productos en una cinta transportadora.
 *
 * \section hardConn Hardware Connection
 *
 * |   Infrarrojo	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	 T_COL0	    |
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2021 | Document creation		                         |
 * | 22/09/2020	| The application is modified by María Casablanca|
 * | 			| 	                     						 |
 *
 * @author Traversaro Julian
 *
 */

#ifndef _PROYECTO_3_INFRARROJO_H
#define _PROYECTO_3_INFRARROJO_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO_3_INFRARROJO_H */

