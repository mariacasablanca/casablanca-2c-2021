/*! @mainpage Proyecto 3 Infrarrojo
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de contar productos en una cinta transportadora.
 *
 * \section hardConn Hardware Connection
 *
 * |   Infrarrojo	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	 T_COL0	    |
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2021 | Document creation		                         |
 * | 22/09/2020	| The application is modified by María Casablanca|
 * | 			| 	                     						 |
 *
 * @author Traversaro Julian
 *

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_3_Infrarrojo.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/

#define TIEMPO_MS 200
#define ON 1
#define OFF 0


/*==================[internal data definition]===============================*/

bool tecla_uno = OFF;
bool tecla_dos=ON;
bool tecla_tres=OFF;
uint8_t teclas;
bool cinta_contadora=OFF;
uint8_t cantidad_obj=0;

/*==================[internal functions declaration]=========================*/
void SisInit(void);
void CuentaCinta(void);
void DatoSerie(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,500,&CuentaCinta};
serial_config UART_USB = {SERIAL_PORT_PC,115200,&DatoSerie};

/*==================[external functions definition]==========================*/

void LedNatABin(uint8_t cantidad_obj){

	switch(cantidad_obj){
	// LED_RGB_B (Azul) como el bit 2**3, el LED_1 como 2**2, el LED_2 como 2**1 y el LED_3 como 2**0.

	case 0:
		LedsOffAll();
		break;

	case 1:
		LedOn(LED_3);
		break;

	case 2:
		LedsOffAll();
		LedOn(LED_2);
		break;

	case 3:
		LedsOffAll();
		LedOn(LED_3);
		LedOn(LED_2);
		break;

	case 4:
		LedsOffAll();
		LedOn(LED_1);
		break;

	case 5:
		LedsOffAll();
		LedOn(LED_1);
		LedOn(LED_3);
		break;

	case 6:
		LedsOffAll();
		LedOn(LED_1);
		LedOn(LED_2);
		break;

	case 7:
		LedsOffAll();
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
		break;

	case 8:
		LedsOffAll();
		LedOn(LED_RGB_B);
		break;

	case 9:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_3);
		break;

	case 10:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_2);
		break;

	case 11:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_3);
		LedOn(LED_2);
		break;

	case 12:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		break;

	case 13:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_3);
		LedOn(LED_1);
		break;

	case 14:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		break;

	case 15:
		LedsOffAll();
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
		break;

	}
}

void TeclaUno(){
	//PRENDO-APAGO
	tecla_uno=!tecla_uno;
	UartSendString(SERIAL_PORT_PC, "Cambia el estado de 'O' \n\r");

}
void TeclaDos(){
	//HOLDEO
	tecla_dos=!tecla_dos;
	UartSendString(SERIAL_PORT_PC, "Cambia el estado de 'H' \n\r");
}

void TeclaTres(){
	//RESET
	tecla_tres=!tecla_tres;
	UartSendString(SERIAL_PORT_PC, "Cambia el estado de '0' \n\r");
}

void SisInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	SwitchActivInt(SWITCH_1, TeclaUno);
	SwitchActivInt(SWITCH_2, TeclaDos);
	SwitchActivInt(SWITCH_3, TeclaTres);
	UartInit(&UART_USB);
}

void CuentaCinta(void){


	if(tecla_uno && cantidad_obj <16){
		cinta_contadora	= Tcrt5000State();

		if(cinta_contadora){
			cantidad_obj=cantidad_obj+1;
		}

		if(tecla_dos){
			LedNatABin(cantidad_obj);
		}

		if(tecla_tres){
			cantidad_obj=0;
			LedsOffAll();
			tecla_tres=!tecla_tres;
		}
	}
	else{
		//VERIFICO EL APAGADO DE TEC1, PARPADEANDO EN ROJO RGB
		LedOn(LED_RGB_R);
		DelayMs(TIEMPO_MS);
		LedOff(LED_RGB_R);
		DelayMs(TIEMPO_MS);
		cantidad_obj=0;
		LedsOffAll();
	}
}
void DatoSerie(void){

	uint8_t dato_teclado;

	UartReadByte(SERIAL_PORT_PC, &dato_teclado);

	switch(dato_teclado){

	case 'O':
		TeclaUno();
		break;

	case 'H':
		TeclaDos();
		break;

	case '0':
		TeclaTres();
		break;
	}

}
int main(void){

	SisInit();

	while(1){

		/*Nada*/
	}

	return 0;

}




/*==================[end of file]============================================*/

