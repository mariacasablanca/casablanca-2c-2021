/*! @mainpage Proyecto complementario
 *
 * \section genDesc General Description
 *
 * This application measures distance with an ultrasound, and show it in an LCD Display.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |   Display  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 D1  	    |  	  LCD1	    |
 * | 	 D2	        | 	  LCD2		|
 * | 	 D3         | 	  LCD3   	|
 * |   	 D4         |  	  LCD4 		|
 * |   	 SEL_0      |  	  GPIO1		|
 * |   	 SEL_1      |  	  GPIO4		|
 * |     SEL_2      |  	  GPIO5		|
 * | 	 +5V 	 	| 	  +5V    	|
 * | 	 GND	   	| 	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author María Casablanca
 *
 */

#ifndef _PROYECTO_COMPLEMENTARIO_H
#define _PROYECTO_COMPLEMENTARIO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO_COMPLEMENTARIO_H */

