/*! @mainpage Proyecto complementario
 *
 * \section genDesc General Description
 *
 * This application maeasures distance with an ultrasound, and show it in an LCD Display.
 *
 * \section hardConn Hardware Connection
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |   Display  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 D1  	    |  	  LCD1	    |
 * | 	 D2	        | 	  LCD2		|
 * | 	 D3         | 	  LCD3   	|
 * |   	 D4         |  	  LCD4 		|
 * |   	 SEL_0      |  	  GPIO1		|
 * |   	 SEL_1      |  	  GPIO4		|
 * |     SEL_2      |  	  GPIO5		|
 * | 	 +5V 	 	| 	  +5V    	|
 * | 	 GND	   	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 08/09/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Maria Casablanca
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_complementario.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "delay.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

gpio_t pins_config[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};
int16_t distancia  = 0;
uint8_t teclas;
uint8_t estado = 0;
uint8_t estado_teclas_actual = 0;
uint8_t estado_teclas_anterior = 0;
uint8_t hold = 0;

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	/** Inicialización de los drivers  */

	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	ITSE0803Init(pins_config);
	SwitchesInit();

	/** Programa principal  */

	while(1){

		teclas  = SwitchesRead();

		if(estado_teclas_actual!=estado_teclas_anterior){
			DelayUs(1);
			estado_teclas_anterior = estado_teclas_actual;
		} //No es necesario, ya es suficiente con el delay del final


		switch(teclas){
		case SWITCH_1:

			estado = !estado;

			break;

		case SWITCH_2:

			hold = !hold;

			break;

		}

		if(estado){

			distancia = HcSr04ReadDistanceCentimeters();

			if(!hold){

				LedOn(LED_RGB_B);

				if(distancia<10){
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				else{
					LedOn(LED_1);
					if(distancia<20){
						LedOff(LED_2);
						LedOff(LED_3);
					}
					else{
						LedOn(LED_2);
						if(distancia<30){
							LedOff(LED_3);
						}
						else{
							LedOn(LED_3);
						}
					}
				}

				ITSE0803DisplayValue(distancia);
			}

			distancia = 0;
		}

		else{
			ITSE0803DisplayValue(0);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			LedOff(LED_RGB_B);
			hold = 0;
		}
		DelaySec(1);
	}

	return 0;
}

/*==================[end of file]============================================*/

