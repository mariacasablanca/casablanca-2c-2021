/*! @mainpage Proyecto 3 Ultrasonido
 *
 * \section genDesc General Description
 *
 *Esta aplicación permite conectar la EDU-CIAA con la PC mediante el bridge UART-USB y luego enviar, por este canal de comunicación, los datos de medida obtenidos del sensor de distancia ultrasónico HC-SR04.
 *
 * \section hardConn Hardware Connection
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |   Display  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 D1  	    |  	  LCD1	    |
 * | 	 D2	        | 	  LCD2		|
 * | 	 D3         | 	  LCD3   	|
 * |   	 D4         |  	  LCD4 		|
 * |   	 SEL_0      |  	  GPIO1		|
 * |   	 SEL_1      |  	  GPIO4		|
 * |     SEL_2      |  	  GPIO5		|
 * | 	 +5V 	 	| 	  +5V    	|
 * | 	 GND	   	| 	  GND		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * | 22/09/2021	| A functionality is added	                     |
 *
 * @author Maria Casablanca
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto_3_Ultrasonido/inc/Proyecto_3_Ultrasonido.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

gpio_t pins_config[7]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};
int16_t distancia  = 0;
uint8_t estado = 0;
uint8_t hold = 0;

/*==================[internal functions declaration]=========================*/
void Medir(void);
void MedirSerie(void);
void SisInit(void);

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&Medir};
serial_config UART_USB = {SERIAL_PORT_PC,115200,&MedirSerie};

/*==================[external functions definition]==========================*/
void TeclaUno(){
	estado = !estado;
	UartSendString(SERIAL_PORT_PC, "Cambia el estado de 'O' \n\r");
}

void TeclaDos(){
	hold = !hold;
	UartSendString(SERIAL_PORT_PC, "Cambia el estado de 'H' \n\r");
}


void SisInit(void)
{
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	ITSE0803Init(pins_config);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, TeclaUno);
	SwitchActivInt(SWITCH_2, TeclaDos);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);
}

void Medir(void){


	if(estado){

		distancia = HcSr04ReadDistanceCentimeters();

		if(!hold){

			LedOn(LED_RGB_B);

			if(distancia<10){
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			else{
				LedOn(LED_1);
				if(distancia<20){
					LedOff(LED_2);
					LedOff(LED_3);
				}
				else{
					LedOn(LED_2);
					if(distancia<30){
						LedOff(LED_3);
					}
					else{
						LedOn(LED_3);
					}
				}
			}

			ITSE0803DisplayValue(distancia);
			UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10));
			UartSendString(SERIAL_PORT_PC, " cm\n\r");
		}

		distancia = 0;

	}

	else{
		ITSE0803DisplayValue(0);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
		LedOff(LED_RGB_B);
		hold = 0;
	}
}

void MedirSerie(void){

	uint8_t dato_teclado;

	UartReadByte(SERIAL_PORT_PC, &dato_teclado);

	switch(dato_teclado){

	case 'O':
		TeclaUno();
		break;

	case 'H':
		TeclaDos();
		break;
	}

}

int main(void){

	SisInit();

	while(1){

		/*Nada*/
	}

	return 0;
}

/*==================[end of file]============================================*/

