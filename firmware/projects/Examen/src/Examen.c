/*! @mainpage Proyecto Parcial
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de detectar objetos cercanos (sonar) en un robot móvil.
 * Utiliza un HC-SR04 para el sensado de los objetos, el cual se encuentra sobre
 * un potenciómetro solidario al motor mediante engranajes. Esto permite conocer
 * la orientación del HC-SR04 a partir de medir el voltaje en el punto medio de
 * dicho potenciómetro.
 *
 * \section hardConn Hardware Connection
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |  Potenciometer	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  VCC	    | 	   +5V	    |
 * | 	  OUT 	    | 	   CH1	    |
 * | 	  GND 	 	| 	   GND  	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Casablanca María
 *

/*==================[inclusions]=============================================*/
#include "../inc/Examen.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "gongiometro.h"
#include "hc_sr4.h"

/*==================[macros and definitions]=================================*/

#define TIEMPO_MS 500    /* <= Período al cual funionará el timer */
#define CH1 2            /* <= Canal al cual se conecta el punto medio del potenciometro */
#define BIT_0 0			 /* <= Definición posición bit 0 */
#define BIT_1 1 		 /* <= Definición posición bit 1 */
#define CANT_POS 6		 /* <= Definición de la cantidad de posiciones en las cuales se mide la distancia */

/*==================[internal data definition]===============================*/

uint8_t angulo = 0;		  		/* <= Angulo medidio por el gongiometro, que me da la posición del sensor */
uint8_t numero_medicion = 0;    /* <= Cuenta la cantidad de mediciones que se van realizando por ciclo */
bool gira_derecha = TRUE; 		/* <= Me permite controlar si el robot gira hacia la derecha o la izquierda */

/*==================[internal functions declaration]=========================*/
void Control(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,TIEMPO_MS,&Control};
serial_config UART_USB = {SERIAL_PORT_PC,115200,NULL};

/*==================[external functions definition]==========================*/


void SisInit(void)
{
	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	LedsInit();
	SwitchesInit();
	GongioInit(CH1);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);
}


void Control(void){

	angulo = GongioMedirAngulo();
	uint8_t distancia[6];
	uint8_t arreglo_angulos[6];
	uint8_t mask_1 = 0;
	uint8_t mask_2 = 0;

	if(angulo%15 == 0){
		mask_1 = mask_1 | (1<<BIT_0);
	}
	if(angulo >= 15 ){
		mask_1 = mask_1 | (1<<BIT_1);
	}
	if(angulo == 0){
		mask_2 = 1<<BIT_0;
	}

	if(mask_1 == 3 || mask_2 == 1){

		distancia[numero_medicion] = HcSr04ReadDistanceCentimeters();
		arreglo_angulos[numero_medicion] = angulo;
		UartSendString(SERIAL_PORT_PC, UartItoa(angulo, 10));
		UartSendString(SERIAL_PORT_PC, "° objeto a ");
		UartSendString(SERIAL_PORT_PC, UartItoa(distancia[numero_medicion], 10));
		UartSendString(SERIAL_PORT_PC, " cm\n\r");
		numero_medicion++;
		//[grado]º objeto a [distancia] cm

		mask_1 = 0;
		mask_2 = 0;

	}

	if(gira_derecha == TRUE){

		LedOn(LED_RGB_R);  //Enciendo LED rojo RGB
		LedOff(LED_RGB_G); //Apago LED Verde RGB

	}
	else{

		LedOff(LED_RGB_G);  //Apago LED rojo RGB
		LedOn(LED_RGB_R); //Enciendo LED Verde RGB

	}

	if(angulo>=75){
		gira_derecha = FALSE;

		uint8_t indice = 0;
		uint8_t max = distancia[0];
		uint8_t aux = 0;
		uint8_t posicion = 0;
		for(indice = 0;indice<numero_medicion;indice++){

			aux = distancia[indice];
			if(aux>max){
				max = aux;
				posicion = indice;
			}
		}

		UartSendString(SERIAL_PORT_PC, "Obstáculo en: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(arreglo_angulos[posicion], 10));
		UartSendString(SERIAL_PORT_PC, "°\n\r");
		numero_medicion = 0;
	}
	if(angulo==0){

		gira_derecha = TRUE;

		uint8_t indice = 0;
		uint8_t max = distancia[0];
		uint8_t aux = 0;
		uint8_t posicion = 0;
		for(indice = 0; indice<numero_medicion; indice++){

			aux = distancia[indice];
			if(aux>max){
				max = aux;
				posicion = indice;
			}
		}

		UartSendString(SERIAL_PORT_PC, "Obstáculo en: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(arreglo_angulos[posicion], 10));
		UartSendString(SERIAL_PORT_PC, "°\n\r");
		numero_medicion = 0;
	}

}


int main(void){

	SisInit();
	while(1){

		/*Nada*/
	}

	return 0;

}




/*==================[end of file]============================================*/

