/*! @mainpage Proyecto Parcial
 *
 * \section genDesc General Description
 *
 * Este sistema es capaz de detectar objetos cercanos (sonar) en un robot móvil.
 * Utiliza un HC-SR04 para el sensado de los objetos, el cual se encuentra sobre
 * un potenciómetro solidario al motor mediante engranajes. Esto permite conocer
 * la orientación del HC-SR04 a partir de medir el voltaje en el punto medio de
 * dicho potenciómetro.
 *
 * \section hardConn Hardware Connection
 *
 * |   Ultrasound	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3		|
 * | 	+5V 	 	| 	  +5V    	|
 * | 	GND	    	| 	  GND		|
 *
 * |  Potenciometer	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  VCC	    | 	   +5V	    |
 * | 	  OUT 	    | 	   CH1	    |
 * | 	  GND 	 	| 	   GND  	|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Casablanca María
 *
*/

#ifndef _EXAMEN_H
#define _EXAMEN_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EXAMEN_H */

