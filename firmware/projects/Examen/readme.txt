Examen. Conexionado:

 * |   Ultrasound	|   EDU-CIAA	    |
 * |:------------------:|:------------------|
 * | 	ECHO	 	| 	 T_FIL2	    |
 * | 	TRIGGER	 	| 	 T_FIL3	    |
 * | 	+5V 	 	| 	  +5V       |
 * | 	GND	    	| 	  GND	    |
 *
 * |  Potenciometro	|   EDU-CIAA	    |
 * |:------------------:|:------------------|
 * | 	  VCC	        | 	   +5V	    |
 * | 	  OUT 	        | 	   CH1	    |
 * | 	  GND 	 	| 	   GND      |