/*! @mainpage Proyecto_Final
 *
 * \section genDesc General Description
 *
 * Esta aplicación mide la temperatura y la humedad relativa dentro de una cámara,
 * y regula dichos parámetros según los requerimientos del usuario,
 * prendiendo y apagando sistemas de refrigeración y calefaccionamiento para la temperatura,
 * y humidificadores y secadores para la humedad (en este caso simulado por LEDS).
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  Sensor  TyH	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	GPIO_LCD_1	|
 * | 	  PIN2	 	| 	  +3,3V		|
 * | 	  PIN3	 	| 	   CH1		|
 * | 	  PIN4	 	| 	   CH2		|
 * | 	  PIN5	 	| 	   GND		|

 * |  MonsterMoto	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  INA1	 	|     GPIO_1    |
 * | 	  INB1	 	| 	  GPIO_3   	|
 * | 	  PWM1	 	| 	  TFIL_2    |
 * | 	  EN1	 	| 	  GPIO_5 	|
 * | 	  CS1	 	| 	   CH3 		|
 * | 	  5V	 	| 	   +5V		|
 * | 	  GND	 	| 	   GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author María Casablanca
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Final.h"       /* <= own header */
#include "Si7007.h"
#include "timer.h"
#include "uart.h"
#include "sapi_rtc.h"
#include "led.h"
#include "systemclock.h"
#include "pwm_sct.h"
#include "VNH3SP30.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define BIT_0 0
#define BIT_1 1
#define BIT_2 2
#define BIT_3 3

/*==================[internal data definition]===============================*/

float measured_temperature;         	/* <= Temperatura medida por el sensor */
float measured_humidity;            	/* <= Humedad medida por el sensor */
float temperature;                   	/* <= Valor de temperatura establecido por usuario*/
float humidity;                 	    /* <= Valor de humedad establecido por usuario*/
float max_temperature;              	/* <= Valor de temperatura máximo del rango establecido */
float min_temperature;              	/* <= Valor de temperatura mínimo del rango establecido */
float max_humidity;                 	/* <= Valor de humedad máximo del rango establecido */
float min_humidity;                 	/* <= Valor de humedad mínimo del rango establecido */
float alfa = 0.5;						/* <= Valor de alfa para la ecuacion del filtrado */

uint8_t complete_cofig = 0;             /* <= Máscara para controlar que se configuraron todas las variables*/
uint8_t duration_mins;					/* <= Tiempo de la simulacion en minutos entre 1 y 60 */
uint8_t duration_hours;                 /* <= Tiempo de la simulacion en horas */
uint8_t duration_days;                  /* <= Tiempo de la simulacion en dias */
uint8_t initial_temp;                   /* <= Primer valor de temperatura medido por el sensor */
uint8_t temp_filtrada = 0;              /* <= Valor de temperatura filtrado */
uint8_t temp_filtrada_ant = 0;          /* <= Valor de temperatura filtrado anterior*/

bool apagado = FALSE;                   /* <= Verifica si el motor ya se encuentra apagado */
bool time_finished = FALSE;				/* <= Verifica si finalizó el tiempo */
bool time_finished_aux = FALSE;         /* <= Verifica si el usuario presiono la tecla F para finalizar manualmente */
bool time_started = FALSE;              /* <= Verifica si inició el tiempo */
bool set_temperature = FALSE;           /* <= Bandera para el control del menú */
bool set_humidity = FALSE;              /* <= Bandera para el control del menú */
bool set_minutes = FALSE;               /* <= Bandera para el control del menú */
bool set_hours = FALSE;                 /* <= Bandera para el control del menú */
bool warm;                              /* <= Verifica si el sistema debe calentar */
bool cool;                              /* <= Verifica si el sistema debe enfriar */
bool control = FALSE;                   /* <= Bandera para el control del sistema */



/*==================[internal functions declaration]=========================*/

void Measure(void);
void Menu(void);
bool ControlaTiempo();
void Cool(void);
void Warm(void);
void TurnOff(void);

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,1000,&Measure};
serial_config UART_USB = {SERIAL_PORT_PC,115200,&Menu};
rtc_t actual_time_config = {0, 0, 0, 0, 0, 0, 0};
Si7007_config TyH_config = {GPIO_LCD_1, CH1, CH2};
pwm_out_t pwm_pins2[1] = {CTOUT0};

/*==================[external functions definition]==========================*/

void Warm(void){
	VNH3SP30_setSpeed(-100);
}

void Cool(void){
	VNH3SP30_setSpeed(100);
}

void TurnOff(void){
	if(!apagado){
		VNH3SP30_setSpeed(0);
		apagado = TRUE;
	}
}

void ShowTime(void){

	/*Muestra el tiempo transcurrido desde que inició el proceso*/

	rtcRead(&actual_time_config);
	UartSendString(SERIAL_PORT_PC, "El tiempo transcurrido es: \n\r");
	UartSendString(SERIAL_PORT_PC, UartItoa(actual_time_config.min, 10));
	UartSendString(SERIAL_PORT_PC, "minutos \n\r");
	UartSendString(SERIAL_PORT_PC, UartItoa(actual_time_config.hour, 10));
	UartSendString(SERIAL_PORT_PC, "horas \n\r");

}
void SisInit(void)
{
	/*Inicialización de drivers y otros*/

	SystemClockInit();
	LedsInit();
	Si7007Init(&TyH_config);
	TimerInit(&my_timer);
	UartInit(&UART_USB);
	SwitchesInit();
	VNH3SP30_begin(pwm_pins2, GPIO_1, GPIO_3, GPIO_5, CH3);
	SwitchActivInt(SWITCH_1, &ShowTime);
	LedsOffAll();
	TurnOff();

}

void Menu(void){

	uint8_t data_keyboard;
	UartReadByte(SERIAL_PORT_PC, &data_keyboard);
	/******* RECUPERO VALORES NUMERICOS **********/
	if(set_temperature){
		temperature = data_keyboard;
		/****** Para la temperatura se verifica el valor dentro de un rango ********/
		if((temperature<=60) && (temperature>=(0))){
			data_keyboard = 0;
			set_temperature = FALSE;
			complete_cofig = complete_cofig | (1<<BIT_0);
			UartSendString(SERIAL_PORT_PC, UartItoa(temperature, 10));
			UartSendString(SERIAL_PORT_PC, " grados \n\r");
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre -0 y 60 grados\r\n");
			data_keyboard = 0;
		}
	}

	if(set_humidity){
		humidity = data_keyboard;
		/****** Para la humedad se verifica el valor dentro de un rango ********/
		if((humidity<=100) && (humidity>=0)){
			data_keyboard = 0;
			set_humidity = FALSE;
			complete_cofig = complete_cofig | (1<<BIT_1);
			UartSendString(SERIAL_PORT_PC, UartItoa(humidity, 10));
			UartSendString(SERIAL_PORT_PC, " %\n\r");
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre 0 y 100\r\n");
			data_keyboard = 0;
		}
	}

	if(set_minutes){
		duration_mins = data_keyboard;
		/****** Para los minutos se verifica el valor dentro de un rango ********/
		if((duration_mins<=256) && (duration_mins>=0)){
			data_keyboard = 0;
			set_minutes = FALSE;
			complete_cofig = complete_cofig | (1<<BIT_2);
			UartSendString(SERIAL_PORT_PC, UartItoa(duration_mins, 10));
			UartSendString(SERIAL_PORT_PC, " minutos\n\r");
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre 1 y 256\r\n");
			data_keyboard = 0;
		}
	}

	if(set_hours){
		duration_hours = data_keyboard;
		/****** Para la variable #3 se verifica el valor dentro de un rango ********/
		if((duration_hours<=256) && (duration_hours>=0)){
			data_keyboard = 0;
			set_hours = FALSE;
			complete_cofig = complete_cofig | (1<<BIT_3);
			UartSendString(SERIAL_PORT_PC, UartItoa(duration_hours, 10));
			UartSendString(SERIAL_PORT_PC, " horas\n\r");

		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre 1 y 256\r\n");
			data_keyboard = 0;
		}
	}

	/******* RECUPERO COMANDOS **********/
	switch(data_keyboard){
	case 'T':
		UartSendString(SERIAL_PORT_PC, "Ingrese la Temperatura deseada (entre 0 y 60°C):");
		set_temperature= TRUE;
		break;
	case 'H':
		UartSendString(SERIAL_PORT_PC, "Ingrese la Humedad relativa deseada (ente 0 y 100%):");
		set_humidity= TRUE;
		break;
	case 'A':
		UartSendString(SERIAL_PORT_PC, "Ingrese la cantidad de minutos de duración:");
		set_minutes = TRUE;
		break;
	case 'B':
		UartSendString(SERIAL_PORT_PC, "Ingrese la cantidad de horas de duración:");
		set_hours = TRUE;
		break;

	case 'M':
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'T' para configurar la Temperatura:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'H' para configurar la Humedad:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'A' para cargar la cantidad de minutos de duración \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'B' para cargar la cantidad de horas de duración \r\n");
		UartSendString(SERIAL_PORT_PC, "Presione TEC1 para ver el tiempo transcurrido:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'I' para INICIAR. Debe configurar todos los valores previamente. \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'F' para FINALIZAR\r\n");
		break;

	case 'I': //Se inicia el programa

		if(complete_cofig==15){

			initial_temp = Si7007MeasureTemperature();

			UartSendString(SERIAL_PORT_PC, "Los parametros establecidos son:\n\r");
			UartSendString(SERIAL_PORT_PC, "Temperatura: ");
			UartSendString(SERIAL_PORT_PC, UartItoa(temperature, 10));
			UartSendString(SERIAL_PORT_PC, " grados \n\r");
			UartSendString(SERIAL_PORT_PC, "Humedad: ");
			UartSendString(SERIAL_PORT_PC, UartItoa(humidity, 10));
			UartSendString(SERIAL_PORT_PC, " % \n\r");
			UartSendString(SERIAL_PORT_PC, "Tiempo en horas: ");
			UartSendString(SERIAL_PORT_PC, UartItoa(duration_hours, 10));
			UartSendString(SERIAL_PORT_PC, "\n\r");
			UartSendString(SERIAL_PORT_PC, "Tiempo en minutos: ");
			UartSendString(SERIAL_PORT_PC, UartItoa(duration_mins, 10));
			UartSendString(SERIAL_PORT_PC, "\n\r");

			max_temperature = temperature + 2; //defino una histersis de 4 grados para temp
			min_temperature = temperature - 2; //defino una histersis de 4 grados para temp

			max_humidity = humidity + 5;       //defino una histeresis del 10% para humedad
			min_humidity = humidity - 5;       //defino una histeresis del 10% para humedad

			/**Convierto a horas si los minutos son mas de 60, y convierto a dias si las horas son mas de 24**/
			duration_hours = duration_hours + duration_mins/60;
			duration_mins = duration_mins%60;
			duration_days = duration_hours/24;
			duration_hours = duration_hours%24;

			/**Se configura el rtc, se activa la bandera del inicio del tiempo y se activa el timer**/
			rtcConfig(&actual_time_config);
			time_started = TRUE;
			TimerStart(TIMER_A);

		}
		else{
			UartSendString(SERIAL_PORT_PC, "Debe configurar las 4 variables para iniciar. \r\n");
		}
		break;

	case 'F': //Finaliza el programa
		time_finished_aux = TRUE;
		break;
	}
}

void Measure(void){

	/*****Verifico si ya inció el tiempo******/
	if(time_started){

		time_finished = ControlaTiempo();      /*Controla si finalizo el tiempo o no*/

		if(time_finished == TRUE){

			//Finaliza el programa.

			TimerStop(TIMER_A);
			TurnOff();
			UartSendString(SERIAL_PORT_PC, " Finaliza el tiempo. \n\r");
			UartSendString(SERIAL_PORT_PC, " Presione M para ver el menu y volver a empezar. \n\r");
			LedsOffAll();
			initial_temp = 0;
			complete_cofig = 0;
		}
		else{
			/*Si el tiempo no finalizó, mido las variables y activo la bandera del control*/

			apagado = FALSE;
			measured_temperature = Si7007MeasureTemperature();
			measured_humidity = Si7007MeasureHumidity();
			control  = TRUE;
		}
	}
}

bool ControlaTiempo(){

	rtcRead(&actual_time_config);

	/*Como todas las variables de la hora inician en 0, el tiempo actual será igual al tiempo transcurrido*/

	uint8_t min = actual_time_config.min;
	uint8_t hour = actual_time_config.hour;
	uint8_t day = actual_time_config.mday;

	/*Comparo los tiempos establecidos con el tiempo transcurrido para las 3 variables*/
	if(min == duration_mins){
		if(hour==duration_hours){
			if(day==duration_days){
				time_finished = TRUE;
				duration_mins = 0;
				duration_hours = 0;
				duration_days = 0;
			}
		}
	}

	if(time_finished_aux == TRUE){
		time_finished = TRUE;
		duration_mins = 0;
		duration_hours = 0;
		duration_days = 0;
	}

	return time_finished;
}

int main(void){

	SisInit();
	UartSendString(SERIAL_PORT_PC, "Ingrese 'M' para ver el menu\r\n");
	while(1){

		if(control){

			/**Filtro para la temperatura**/
			temp_filtrada = temp_filtrada_ant+alfa*(measured_temperature-temp_filtrada_ant);
			temp_filtrada_ant = temp_filtrada;

			/**Se envía por UART los valores de temperatura filtrada, la temperatura medida y la temperatura establecida por el usuario**/
			UartSendString(SERIAL_PORT_PC, UartItoa(temp_filtrada, 10));
			UartSendString(SERIAL_PORT_PC, ",");
			UartSendString(SERIAL_PORT_PC, UartItoa(temperature, 10));
			UartSendString(SERIAL_PORT_PC, ",");
			UartSendString(SERIAL_PORT_PC, UartItoa(measured_temperature, 10));
			UartSendString(SERIAL_PORT_PC, ",");

			/**Se envía por UART los valores de humedad medida y la humedad establecida por el usuario**/
			UartSendString(SERIAL_PORT_PC, UartItoa(measured_humidity, 10));
			UartSendString(SERIAL_PORT_PC, ",");
			UartSendString(SERIAL_PORT_PC, UartItoa(humidity, 10));
			UartSendString(SERIAL_PORT_PC, "\n\r");

			/**En base a la temperatura inicial y la establecida por el usuario, se controla si se necesita enfriar o calentar**/
			if(initial_temp<temperature){
				warm = TRUE;
				cool = FALSE;
			}
			else{
				warm = FALSE;
				cool = TRUE;
			}

			if(warm){
				if(temp_filtrada<min_temperature){
					/**Se prende la calefaccion**/
					Warm();
				}
				else{
					if(temp_filtrada>max_temperature){
						/**Se apaga la calefaccion y se prende el sistema de enfriamiento**/
						TurnOff();
						Cool();
					}
				}
				if(measured_humidity<min_humidity){
					//Se prende humidificador
					LedOn(LED_1);
					//Se apaga secador de humedad
					LedOff(LED_2);
				}
				else{
					//Se apaga la humidificador
					LedOff(LED_1);
					//Se apagan secador de humedad
					LedOff(LED_2);
					if(measured_humidity>max_humidity){
						//se prenden los ventiladores
						LedOn(LED_2);
					}
				}
			}

			if(cool){
				if(temp_filtrada>max_temperature){
					/**Se prende el sistema de enfriamiento**/
					Cool();
				}
				else{
					if(temp_filtrada<min_temperature){
						/**Se apaga el sistema de enfriamiento y se prende la calefaccion**/
						TurnOff();
						Warm();
					}
				}
				if(measured_humidity<min_humidity){
					//Se prende humidificador
					LedOn(LED_3);
					//Se apaga secador de humedad
					LedOff(LED_RGB_B);
				}
				else{
					//Se apaga la humidificador
					LedOff(LED_3);
					//Se apagan secador de humedad
					LedOff(LED_RGB_B);
					if(measured_humidity>max_humidity){
						//se prenden los ventiladores
						LedOn(LED_RGB_B);
					}
				}
			}
			control = FALSE;
		}
	}
	return 0;
}

/*==================[end of file]============================================*/

