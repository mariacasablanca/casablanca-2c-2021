/*! @mainpage Proyecto_Final
 *
 * \section genDesc General Description
 *
 * Esta aplicación mide la temperatura y la humedad relativa dentro de una cámara,
 * y regula dichos parámetros según los requerimientos del usuario,
 * prendiendo y apagando sistemas de refrigeración y calefaccionamiento para la temperatura,
 * y humidificadores y secadores para la humedad (en este caso simulado por LEDS).
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  Sensor  TyH	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  PIN1	 	| 	GPIO_LCD_1	|
 * | 	  PIN2	 	| 	  +3,3V		|
 * | 	  PIN3	 	| 	   CH1		|
 * | 	  PIN4	 	| 	   CH2		|
 * | 	  PIN5	 	| 	   GND		|

 * |  MonsterMoto	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  INA1	 	|     GPIO_1    |
 * | 	  INB1	 	| 	  GPIO_3   	|
 * | 	  PWM1	 	| 	  TFIL_2    |
 * | 	  EN1	 	| 	  GPIO_5 	|
 * | 	  CS1	 	| 	   CH3 		|
 * | 	  5V	 	| 	   +5V		|
 * | 	  GND	 	| 	   GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/10/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author María Casablanca
 *
 */


#ifndef _PROYECTO_FINAL_H
#define _PROYECTO_FINAL_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*============	======[end of file]============================================*/


#endif /* #ifndef _PROYECTO_FINAL_H */

