/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "delay.h"
#include "uart.h"
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



rtc_t hora_config;
typedef enum  {hora, minutos, dia, mes, anio}tiempo_t;

bool set_temperature = FALSE;
uint8_t temperature = 0;
bool set_humidity = FALSE;
uint8_t humidity = 0;
bool set_duration = FALSE;
uint8_t duration = 0;
bool set_clock = FALSE;

tiempo_t time_var = hora;


/*==================[internal functions declaration]=========================*/




void Menu(void){
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);
	/******* RECUPERO VALORES NUMERICOS **********/
	if(set_temperature){
		temperature = dat;
		/****** Para la variable #1 se verifica el valor dentro de un rango ********/
		if((temperature<=60) && (temperature>=(-10))){
			UartSendString(SERIAL_PORT_PC, "Variable #1 (Temperatura):  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
			UartSendString(SERIAL_PORT_PC, "°C \r\n");
			dat = 0;
			set_temperature = FALSE;
			LedOff(LED_1);
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre -10 y 60\r\n");
			dat = 0;
		}
	}
	if(set_humidity){
		humidity = dat;
		/****** Para la variable #2 se verifica el valor dentro de un rango ********/
		if((humidity<=100) && (humidity>=0)){
			UartSendString(SERIAL_PORT_PC, "Variable #2 (Humedad):  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
			UartSendString(SERIAL_PORT_PC, "% \r\n");
			dat = 0;
			set_humidity = FALSE;
			LedOff(LED_2);
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre 0 y 100\r\n");
			dat = 0;

		}
	}
	if(set_duration){

		duration = dat;
		/****** Para la variable #3 se verifica el valor dentro de un rango ********/

		if((duration<=256) && (duration>=0)){

			UartSendString(SERIAL_PORT_PC, "Variable #3 (duración en minutos):  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
			UartSendString(SERIAL_PORT_PC, "min \r\n");
			dat = 0;
			set_duration = FALSE;
			LedOff(LED_3);
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre 1 y 256\r\n");
			dat = 0;

		}
	}

/*	if(set_clock){
		switch(time_var){
		case hora:
			hora_config.hour = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r\n");
			time_var = minutos;
			break;
		case minutos:
			hora_config.min = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el día:  \r\n");
			time_var = dia;
			break;
		case dia:
			hora_config.mday = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes:  \r\n");
			time_var = mes;
			break;
		case mes:
			hora_config.month = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el año:  \r\n");
			time_var = anio;
			break;
		case anio:
			hora_config.year = dat;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			time_var = hora;
			dat = 0;
			set_clock = FALSE;
			LedOff(LED_RGB_B);
			break;
		}

	}*/

	/******* RECUPERO COMANDOS **********/
	switch(dat){
	case 'T':
		UartSendString(SERIAL_PORT_PC, "Ingrese la Temperatura deseada (entre -10 y 60°C):\r\n");
		set_temperature= TRUE;
		LedOn(LED_1);
		break;
	case 'H':
		UartSendString(SERIAL_PORT_PC, "Ingrese la Humedad relativa deseada (ente 0 y 100%):\r\n");
		set_humidity= TRUE;
		LedOn(LED_2);
		break;
	case 'D':
		UartSendString(SERIAL_PORT_PC, "Ingrese la duración del proceso:\r\n");
		set_var_3= TRUE;
		LedOn(LED_3);
		break;
	case 'd':
		UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual:\r\n");
		set_clock= TRUE;
		LedOn(LED_RGB_B);
		break;
	case 'e':
		UartSendString(SERIAL_PORT_PC, "Variable #1: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(temperature, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'm':
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/\r\n");
		//UartSendString(SERIAL_PORT_PC, "Ingrese 'm' para ver el menu:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'T' para configurar la Temperatura:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'H' para configurar la Humedad:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'D' para configurar la duración :\r\n");
		/** etc...*/
		UartSendString(SERIAL_PORT_PC, "Ingrese 'a' para recuperar la variable Temperatura:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'b' para recuperar la variable Humedad:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'c' para recuperar la variable Duración:\r\n");

		/** etc...*/
		UartSendString(SERIAL_PORT_PC, "Ingrese 'd' para configurar la hora:\r\n");
		break;
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();

	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = Menu;
	UartInit(&UART_USB);


	while(1)
	{

	}

	return 0;
}

/*==================[end of file]============================================*/

