/*! @mainpage PRUEBA_ULTRASONDIO
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/prueba_ultrasonido.h"       /* <= own header */
#include "hc_sr4.h"
#include "systemclock.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	int16_t distancia  = 0;

	while(1){

		distancia = HcSr04ReadDistanceCentimeters();
		distancia = HcSr04ReadDistanceInches();


	}

	return 0;
}

/*==================[end of file]============================================*/

