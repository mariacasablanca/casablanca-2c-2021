var group___sensor___ty_h =
[
    [ "Si7007_config", "struct_si7007__config.html", [
      [ "PWM_1", "struct_si7007__config.html#a5b61c0fe8012ce0ec333972f84a74060", null ],
      [ "PWM_2", "struct_si7007__config.html#a321927ba451748d2ddfc9add9a627ef6", null ],
      [ "select", "struct_si7007__config.html#ace3cd9ac850b99064d7a5a8513ca4fb4", null ]
    ] ],
    [ "Si7007Init", "group___sensor___ty_h.html#ga8692b210349b0b2b7fd2ef2ccc4c526a", null ],
    [ "Si7007MeasureHumidity", "group___sensor___ty_h.html#ga0bd8f590867820ba14e6ee5a0ef7ade7", null ],
    [ "Si7007MeasureTemperature", "group___sensor___ty_h.html#gace89737e474b1e9cc93e847351170570", null ]
];