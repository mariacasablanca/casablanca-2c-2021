var searchData=
[
  ['select',['select',['../struct_si7007__config.html#ace3cd9ac850b99064d7a5a8513ca4fb4',1,'Si7007_config']]],
  ['sensor_20temperatura_20y_20humedad',['Sensor Temperatura y Humedad',['../group___sensor___ty_h.html',1,'']]],
  ['serial_5fconfig',['serial_config',['../structserial__config.html',1,'']]],
  ['si7007_5fconfig',['Si7007_config',['../struct_si7007__config.html',1,'']]],
  ['si7007init',['Si7007Init',['../group___sensor___ty_h.html#ga8692b210349b0b2b7fd2ef2ccc4c526a',1,'Si7007Init(Si7007_config *pins):&#160;Si7007.c'],['../group___sensor___ty_h.html#ga8692b210349b0b2b7fd2ef2ccc4c526a',1,'Si7007Init(Si7007_config *pins):&#160;Si7007.c']]],
  ['si7007measurehumidity',['Si7007MeasureHumidity',['../group___sensor___ty_h.html#ga0bd8f590867820ba14e6ee5a0ef7ade7',1,'Si7007MeasureHumidity(void):&#160;Si7007.c'],['../group___sensor___ty_h.html#ga0bd8f590867820ba14e6ee5a0ef7ade7',1,'Si7007MeasureHumidity(void):&#160;Si7007.c']]],
  ['si7007measuretemperature',['Si7007MeasureTemperature',['../group___sensor___ty_h.html#gace89737e474b1e9cc93e847351170570',1,'Si7007MeasureTemperature(void):&#160;Si7007.c'],['../group___sensor___ty_h.html#gace89737e474b1e9cc93e847351170570',1,'Si7007MeasureTemperature(void):&#160;Si7007.c']]],
  ['switch',['Switch',['../group___switch.html',1,'']]],
  ['switchactivint',['SwitchActivInt',['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t tec, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t sw, void *ptr_int_func):&#160;switch.c']]],
  ['switches',['SWITCHES',['../group___switch.html#gaa87203a5637fb4759a378b579aaebff6',1,'switch.h']]],
  ['switchesactivgroupint',['SwitchesActivGroupInt',['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t tecs, void *ptrIntFunc):&#160;switch.c'],['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t switchs, void *ptr_int_func):&#160;switch.c']]],
  ['switchesinit',['SwitchesInit',['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c'],['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c']]],
  ['switchesread',['SwitchesRead',['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c'],['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c']]],
  ['systemclock',['Systemclock',['../group___systemclock.html',1,'']]],
  ['systemclockinit',['SystemClockInit',['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c'],['../group___systemclock.html#ga9a9ce29ac799cb62b7fbfd8040ed77b5',1,'SystemClockInit(void):&#160;systemclock.c']]]
];
